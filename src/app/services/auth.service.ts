import { Injectable } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/compat/auth'
import { Router } from '@angular/router'
import firebase from 'firebase/compat/app'
import { BehaviorSubject } from 'rxjs'
import { User } from 'src/Interfaces/User'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user!: User

  public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)

  constructor(
    private afauth: AngularFireAuth,
    private router: Router
  ) {}

  get isLoggedIn() {
    return this.loggedIn.asObservable()
  }
  
  async login(email:string, password:string) {
    try {
      const user = await this.afauth.signInWithEmailAndPassword(email, password);
      if (!user) await this.afauth.createUserWithEmailAndPassword(email, password)
      localStorage.setItem('user', JSON.stringify(user))
      window.location.href = '/dashboard'
      return
    }
    catch (err) {
      const user = await this.afauth.createUserWithEmailAndPassword(email, password)
      localStorage.setItem('user', JSON.stringify(user))
      window.location.href = '/dashboard'
      return
    }
  }

  async loginWithGoogle() {
    try {
      const user = await this.afauth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      if (!user) return
      localStorage.setItem('user', JSON.stringify(user))
      window.location.href = '/dashboard'
      return
    }
    catch (err) {
      console.log('Error en login:', err)
      return null
    }
  }

  getUserLogued() {
    return this.afauth.authState
  }

  logout() {
    this.afauth.signOut()
  }

  get() {
    try {
      const user = localStorage.getItem('user')
      if (!user) return
      this.loggedIn.next(true)
      this.user = JSON.parse(user)
      console.log(JSON.parse(user))
      console.log(this.user.name)
      return JSON.parse(user)
    }
    catch (err) {
      console.log(err)
      return
    }
  }

  remove() {
    try {
      localStorage.removeItem('user')
      this.logout()
      this.loggedIn.next(false)
      window.location.href = '/'
    }
    catch (err) {
      console.log(err)
    }
  }
}