import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http'
import { Observable } from 'rxjs';
import { IMovie } from 'src/Interfaces/Movies';
import { AngularFirestore } from '@angular/fire/compat/firestore';
@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  api_key:string ="bd5a8e248800f3d8d6f5e50947699035";
  baseURL:string= "https://api.themoviedb.org/3/"
  language:string ='es'
  // params= new HttpParams().set('api_key',this.api_key).set('language',this.language)
  constructor(private _http: HttpClient,private firestore: AngularFirestore) { }
  
  getMovie(): Observable<any>{
    let params= new HttpParams().set('api_key',this.api_key)
    return this._http.get(this.baseURL + 'movie/popular',{params:params})
  }
  getTrending(): Observable<any>{
    let params= new HttpParams().set('api_key',this.api_key)
    return this._http.get(this.baseURL+ 'trending/all/week',{params:params})
  }

  getSeries(): Observable<any>{
    let params = new HttpParams().set('api_key', this.api_key);
    return this._http.get(this.baseURL + 'tv/popular' ,{
      params: params
    })    
  }
  getMovieById(type:string, id:string): Observable<any> {
    let params = new HttpParams().set('api_key', this.api_key);
    return this._http.get(`${this.baseURL}/${type}/${id}`, {params:params})
  }


  async addItem(userId:string, item:IMovie): Promise<any> {
    console.log('entro aca')
    const res = await this.firestore.collection('usuarios').doc(`${userId}`).collection('list').add(item);
    console.log(res)
    return res.id;
  }
  getItem(userId:string): Observable<any> {
    return this.firestore.collection('usuarios').doc(`${userId}`).collection('list').snapshotChanges()
  }
  deleteItem(userId:string, id:string): Promise<any> {
    return this.firestore.collection(`usuarios/${userId}/list`).doc(id).delete()
  }
}

