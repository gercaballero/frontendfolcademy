import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/routes/dashboard/dashboard.component';
import { IngresarComponent } from './components/routes/ingresar/ingresar.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { PelisComponent } from './components/routes/pelis/pelis.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { DetallesComponent } from './components/routes/detalles/detalles.component';
import { ListaComponent } from './components/routes/lista/lista.component';

const routes: Routes = [
  {
    path:'Inicio', component: InicioComponent
  },
  {
    path:'Peliculas',
    component: PelisComponent
  },
  {
    path:'Series',
    component:SeriesComponent
  },
  {
    path:'Ingresar',
    component:IngresarComponent
  },
  {
    path:'dashboard',
    component:DashboardComponent
  },
  {
    path:'lista',
    component:ListaComponent
  },
  { 
    path: 'movie/:id',
    component: DetallesComponent
  },
  { path: 'tv/:id',
  component: DetallesComponent},
  {
    path:'**',
    redirectTo:'Inicio'
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
