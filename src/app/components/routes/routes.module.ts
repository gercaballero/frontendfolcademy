import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { PelisComponent } from './pelis/pelis.component';
import { SeriesComponent } from './series/series.component';
import { IngresarComponent } from './ingresar/ingresar.component';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxBootstrapIconsModule, allIcons } from 'ngx-bootstrap-icons';
import { CardComponent } from '../shared/card/card.component';
import { SharedModule } from '../shared/shared.module';

import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DetallesComponent } from './detalles/detalles.component';
import { ListaComponent } from './lista/lista.component';
@NgModule({
  declarations: [
    InicioComponent,
    PelisComponent,
    SeriesComponent,
    IngresarComponent,
    DashboardComponent,
    DetallesComponent,
    ListaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TabsModule.forRoot(),
    SharedModule,
    ReactiveFormsModule,
    RouterModule
  ],

  bootstrap: [InicioComponent],
  exports: [InicioComponent,SeriesComponent]

})
export class RoutesModule { }
