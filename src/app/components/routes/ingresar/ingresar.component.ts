import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css'],
  // providers: [AuthService,FormBuilder,Router]
})
export class IngresarComponent implements OnInit {

  loginForm: FormGroup = this.fb.group ({
    email: [, [Validators.required, Validators.email]],
    password: [, [Validators.required, Validators.minLength(8)]]
  })

  public password = {
    show: false,
    type: 'password',
    icon: '8'
  }

  campoEsValido(campo: string) {
    return (
      this.loginForm.controls[campo].errors &&
      this.loginForm.controls[campo].touched
    );
  }

  public showPassword = () => {
    if (this.password.show) {
      this.password.type = 'text'
      this.password.icon = '7'
    }
    else {
      this.password.type = 'password'
      this.password.icon = '8'
    }
    this.password.show = !this.password.show
  }

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if(this.authService.get() !== undefined) this.router.navigate(['/'])
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      return;
    }

    this.authService.login(this.loginForm.value.email, this.loginForm.value.password) 
  }

  ingresarConGoogle() {
    this.authService.loginWithGoogle()
  }
}
