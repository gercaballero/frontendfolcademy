import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { IMovie, IMovies, Result } from 'src/Interfaces/Movies';
import { BackgroundComponent } from '../../shared/background/background.component';
import { BotonesComponent } from '../../shared/botones/botones.component';


@Component({
  selector: 'app-pelis',
  templateUrl: './pelis.component.html',
  styleUrls: ['./pelis.component.css'],

  providers:[MoviesService],

})
export class PelisComponent implements OnInit {

  constructor(private _MovieServicesService: MoviesService) { }
  filter='movie'
  ngOnInit(): void {
    this.getMovies();
  }
  movies: IMovie[]=[]
  toSearch="";
  total=0;
  getMovies(){
    this._MovieServicesService.getMovie().subscribe({
      next: (data: Result) => {
        console.log(data.results);
        this.movies=data.results;
        this.total=data.total_results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La peticion termino')
      }
      
    })
  }
  search(value:string=""){
    console.log(value)
    if(value!=" "){
      this.movies = this.movies.filter(elemet => elemet.title?.toLowerCase().includes(value)||elemet.name?.includes(value));
    }
  }
}
