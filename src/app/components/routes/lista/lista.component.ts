import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';
import { IMovies } from 'src/Interfaces/Movies';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  userId!: string;
  movies_series: IMovies = []
  filter='all'
  constructor(
    private User: AuthService,
    private _moviesService: MoviesService
  ) { }

  async ngOnInit() {
    const user = await this.User.get()
    if (user) {
      this.userId = user.user.uid
      this._moviesService.getItem(this.userId).subscribe(res => {
        this.movies_series = []
        res.forEach((element:any) => {
          this.movies_series.push({...element.payload.doc.data()})
        })
      })
    }
  }

}
