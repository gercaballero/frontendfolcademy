import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { IMovie, Result } from 'src/Interfaces/Movies';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {
  constructor(private _MovieServicesService: MoviesService) { }

  ngOnInit(): void {
    this.getMovies();
  }
  series: IMovie[]=[]
  toSearch="";
  total=0;
  filter='tv'
  getMovies(){
    this._MovieServicesService.getSeries().subscribe({
      next: (data: Result) => {
        console.log(data.results);
        this.series=data.results;
        this.total=data.total_results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La peticion termino')
      }
      
    })
  }
  search(value:string=""){
    console.log(value)
    if(value!=" "){
      this.series = this.series.filter(elemet => elemet.title?.toLowerCase().includes(value)||elemet.name?.includes(value));
    }
  }
}
