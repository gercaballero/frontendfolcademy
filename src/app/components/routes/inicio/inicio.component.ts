
import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { TabsetConfig } from 'ngx-bootstrap/tabs';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BotonesComponent } from '../../shared/botones/botones.component';
import { MoviesService } from 'src/app/services/movies.service';
import { IMovie, IMovies, Result } from 'src/Interfaces/Movies';

import { BackgroundComponent } from '../../shared/background/background.component';
import { SearchComponent } from '../../shared/search/search.component';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers:[MoviesService,BackgroundComponent,BotonesComponent, SearchComponent],
})
export class InicioComponent {
  public movies_series: IMovies = []
  @ViewChild('miFormulario') miFormulario!: NgForm;

  initForm = {
    search: ''
  };
  constructor(private _MovieServicesService: MoviesService,private User: AuthService) { 
  }
  async ngOnInit(): Promise<void> {        
    this.getTrending();
    this.funcionFiltrado();
    const user = await this.User.get()
    if (user) this.userId = user.user.uid
  }

  guardar() {
    console.log( this.miFormulario.value.search);
    console.log('Posteo correcto');
    this.funcionFiltrado()
  }
 
  recibirTipo(mensaje: string){ // ESTA FUNCION RECIBE EL TIPO O FILTRO DESDE EL COMPONENTE BOTON
    this.filter= mensaje;
    this.traduccion();
    this.funcionFiltrado();
  }
  traduccion(){
    if (this.filter=='all'){
      this.filtro='Todos';
    }else if (this.filter=='movie'){
      this.filtro='Peliculas'
    }else if (this.filter=='tv'){
      this.filtro='Series'
    }
  }
  filter: string="all";
  filtro: string= 'Todos'
  total=0;
  trendingColeccion: IMovie[] = [];
  moviesColeccion: IMovie[] = [];
  seriesColeccion: IMovie[] = [];
  todosColeccion:IMovie[] = [];
  public showList: IMovies = []
  movies:IMovie[]=[];
  series:IMovie[]=[];
  public userId: string = ''

  


  getTrending(){
    this._MovieServicesService.getTrending().subscribe({
      next: (data: Result) => {
        console.log(data.results);
        this.trendingColeccion=data.results;
        this.total=data.total_results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La peticion termino')
        this.todosColeccion=this.trendingColeccion;
        this.search()
        this.miFormulario.resetForm({
          search: '',
        });
      }
      
    })
  }

  getMovies(){
    this._MovieServicesService.getMovie().subscribe({
      next: (data: Result) => {
        console.log(data.results);
        this.movies=data.results;
        this.total=data.total_results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La peticion termino')
        this.todosColeccion=this.movies;
        this.search()
        this.miFormulario.resetForm({
          search: '',
        });
      }
      
    })
  }

  getSeries(){
    this._MovieServicesService.getSeries().subscribe({
      next: (data: Result) => {
        console.log(data.results);
        this.series=data.results;
        this.total=data.total_results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La peticion termino')
        this.todosColeccion=this.series;
        this.search()
        this.miFormulario.resetForm({
          search: '',
        });
      }
      
    })
  }

  
  toSearch="";

//   funcionFiltrado(){
//     let j=0;
//     if(this.filter!='Todos'){
//       if(this.filter=='Peliculas'){
//         for (let i = 0; i < this.trendingColeccion.length; i++){
//           if(this.trendingColeccion[i].media_type=='movie'){
//             this.moviesColeccion[j]=this.trendingColeccion[i];
//             j++;
//           }
//         }
//         this.todosColeccion=this.moviesColeccion;
//       }
//       else if(this.filter=='Series'){
//           for (let i = 0; i < this.trendingColeccion.length; i++){
//             if(this.trendingColeccion[i].media_type=='tv'){
//               this.seriesColeccion[j]=this.trendingColeccion[i];
//               j++;
//             }
//         }
//         this.todosColeccion=this.seriesColeccion;
//       }
//     }
//     else{
//       this.todosColeccion=this.trendingColeccion;
//     }
//     j=0;
//     console.log(this.todosColeccion);
//     this.total= this.todosColeccion.length;
// } 

funcionFiltrado(){
  let j=0;
  if(this.filter!='all'){
    if(this.filter=='movie'){
      this.getMovies();
      this.todosColeccion=this.movies;
    }
    else if(this.filter=='tv'){
      this.getSeries();
      this.todosColeccion=this.series;
    }
  }
  else{
    this.getTrending();
    this.todosColeccion=this.trendingColeccion;
  }
  j=0;
  console.log(this.todosColeccion);
  // this.total= this.todosColeccion.length;
} 

search(){
  if(this.miFormulario.value.search!=" "){
    this.todosColeccion = this.todosColeccion.filter(elemet => elemet.title?.toLowerCase().includes(this.miFormulario.value.search)||elemet.name?.includes(this.miFormulario.value.search));
    console.log(this.todosColeccion);
  }
  else{
    this.funcionFiltrado();
    console.log(this.todosColeccion);
    console.log('entro aca')
  }
}

}
