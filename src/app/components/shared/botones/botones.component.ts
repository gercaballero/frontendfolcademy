import { Component,Input, Output, EventEmitter, OnInit} from '@angular/core';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { Event } from '@angular/router';
@Component({
  selector: 'app-botones',
  templateUrl: './botones.component.html',
  styleUrls: ['./botones.component.css']
})
export class BotonesComponent implements OnInit{
  public radioModel= 'all';
  @Output() tipo: EventEmitter<string>;
  constructor() { 
    this.tipo = new EventEmitter();
  }
  ngOnInit(): void {
      
  }
  emitirTipo(){
    // Usando la variable emitimos el valor que queremos enviar
    this.tipo.emit(this.radioModel);
  }
  
}