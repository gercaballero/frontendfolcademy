import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { FormsModule } from '@angular/forms';
import { SearchComponent } from './search/search.component';
import { BotonesComponent } from './botones/botones.component';
import { BackgroundComponent } from './background/background.component';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { allIcons, NgxBootstrapIconsModule } from 'ngx-bootstrap-icons';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { RoutesModule } from '../routes/routes.module';

@NgModule({
  declarations: [
    CardComponent,
    SearchComponent,
    BotonesComponent,
    BackgroundComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ButtonsModule.forRoot(),
    FormsModule,
    NgxBootstrapIconsModule.pick(allIcons),
    TabsModule.forRoot(),
    // RoutesModule
  ],
  exports: [CardComponent,BotonesComponent,BackgroundComponent,SearchComponent],
  bootstrap: [CardComponent,BotonesComponent,BackgroundComponent,SearchComponent]
})
export class SharedModule { }
