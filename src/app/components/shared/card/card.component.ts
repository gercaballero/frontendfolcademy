import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';
import { IMovie } from 'src/Interfaces/Movies';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  providers: [MoviesService]
})
export class CardComponent implements OnInit {

  @Input() movie: any;
  movieImg: string | undefined;
  @Input() type!: string;

  @Input() userId:string = ''
  added:boolean = false;
  dbMovieId!: string;
  prueba='hola';

  constructor(private router: Router,private _moviesService: MoviesService) { }

  ngOnInit(): void {
    this.movieImg = 'https://www.themoviedb.org/t/p/w220_and_h330_face' + this.movie.poster_path;
    this.getMoviesIdList()
    console.log(this.movie)
  }

  addMovie(movieId:number) {
    console.log(movieId)
  }
  public goTo(mediaType:string, id:number|string) {
    let type = mediaType
    if (this.type !== 'all') {
      type = this.type
    }
    console.log(this.type)
    this.router.navigate([`${type}/${id}`])
  }

  async handleListClick() {
    console.log('entrooo')
    if (this.added) {
      this._moviesService.deleteItem(this.userId,this.dbMovieId.toString())
      this.added = !this.added
    }
    else {
      const res = await this._moviesService.addItem(this.userId,this.movie)
      this.dbMovieId = res
      this.added = !this.added
    }
  }
  async getMoviesIdList() {
    if (this.userId !== '') {
      this._moviesService.getItem(this.userId).subscribe(res => {
        res.forEach((element:any) => {
          if (element.payload.doc.data().id === this.movie.id) {
            this.added = true
            this.dbMovieId = element.payload.doc.id
          }
        })
      })
    }
  }
}
