export interface Card{
    name:        string;
    id:          number;
    description: string;
    image:         string;
    rating:      string;
    category:    string;
}
