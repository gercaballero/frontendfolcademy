// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBaV4OYM-qoAIuWQ5UuhByZQCy7mRyFLD0",
    authDomain: "metflix-b99ab.firebaseapp.com",
    projectId: "metflix-b99ab",
    storageBucket: "metflix-b99ab.appspot.com",
    messagingSenderId: "273994372562",
    appId: "1:273994372562:web:20e50711e023b98f32640b",
    measurementId: "G-BZD8K9TNLR"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
